<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Settings | ISabiCode.com</title>
</head>
<body>
    <h1>Settings</h1>
    <p>The users settings page</p>
    <table border="1">
        <tr>
            <td>Age:</td>
            <td><?php echo $_SESSION['age'] ?></td>
        </tr>
        <tr>
            <td>Gender:</td>
            <td><?php echo $_SESSION['gender'] ?></td>
        </tr>
        <tr>
            <td>Has Mental Case:</td>
            <td><?php echo $_SESSION['isStupid'] ?></td>
        </tr>
    </table>
</body>
</html>
<?php
    // session_destroy();
?>