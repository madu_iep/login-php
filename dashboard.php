<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard | ISabaCode.com</title>
</head>
<body>
    <h1>Dashboard</h1>
    
    <br>
    <a href="settings.php">Settings</a>

    <h1>Sessions</h1>

    <?php
        $_SESSION['age'] = 30;
        $_SESSION['gender'] = 'Male';
        $_SESSION['hair_color'] = 'Black';
        $_SESSION['isStupid'] = 'Yes!, very stupid';
    ?>
</body>
</html>